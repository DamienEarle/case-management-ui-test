﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    internal class OperationTestSuite : OperationPageObjects
    {
        private readonly LiveQHelperClass _liveQHelperClass = new LiveQHelperClass();

        private IWebElement GetOperationFromList(string operationType)
        {
            return
                PropertiesCollection.AndroidDriver.FindElements(
                    By.XPath("//android.view.View[@content-desc='" + operationType + "']"))[0];
        }

        public void OpenFirstOperationOfType(string operationType)
        {
            var operation = GetOperationFromList(operationType);
            operation.Click();
            //ClickOpenOperationButton();
        }

        private void ClickOpenOperationButton()
        {
            var btnOpenOperation =
                PropertiesCollection.AndroidDriver.FindElement(By.XPath("//android.view.View[@content-desc]='Open Operation'"));
            btnOpenOperation.Click();
        }

        public void SubmitOperation()
        {
            _liveQHelperClass.SubmitOperation();
        }

        public void CreateEncounterFromVRMLookup(string vrm)
        {
            var fieldName = "VRM*";
            _liveQHelperClass.CompleteTextField(fieldName, vrm);
        }

        public void OpenOperationsTab()
        {
            _liveQHelperClass.ClickTab("Operations");
        }
    }
}
