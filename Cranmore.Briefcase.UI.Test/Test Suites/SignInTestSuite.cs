﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cranmore.Briefcase.UI.Test.Object_Models;
using System.Configuration;
using System.IO;
using XMLEngine;

namespace Cranmore.Briefcase.UI.Test.Test_Suites
{
    public class SignInTestSuite : SignInPageObjects
    {
        private HelperClass _helperClass = new HelperClass();

        public void Login(string username = "dva", string password = "dva")
        {
            TxtUsername.SendKeys(username);
            TxtPassword.SendKeys(password);
            BtnLogIn.Click();
        }

        public void LoginWithXml(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            PerformActions(xml);
        }

        private void GetCredentials(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> properties)
        {
            var username = _helperClass.GetPropertyValue(properties.Value, "Username");
            var password = _helperClass.GetPropertyValue(properties.Value, "Password");
            Login(username, password);
        }

        private void PerformActions(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            foreach(var property in xml)
            {
                switch (property.Key)
                {
                    case "GetCredentials":
                        GetCredentials(property);
                        return;
                }
            }         
        }              
        

    }
}
