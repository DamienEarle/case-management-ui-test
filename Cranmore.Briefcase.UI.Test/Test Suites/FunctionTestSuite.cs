﻿using Cranmore.Briefcase.UI.Test.Models;
using Cranmore.Briefcase.UI.Test.Object_Models;
using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Cranmore.Briefcase.UI.Test.Test_Suites
{
    internal class FunctionTestSuite : FunctionPageObjects
    {
        private HelperClass _helperClass = new HelperClass();

        private void ClickAddFunctionButton()
        {
            BtnAddFunction.Click();
        }

        private void ClickCancelButton()
        {
            BtnCancel.Click();
        }

        private void ClickSaveButton()
        {
            BtnSave.Click();
        }

        public void AddFunction(Function function)
        {
            _helperClass.ClickMenuItem("Function", "Add New Function");
            BtnAddFunction.Click();
            SetFunctionData(function);
            BtnAddFunction.Click();
        }

        public void DeleteFunction(Function function)
        {
            _helperClass.ClickMenuItem("Function");
            ClickFunctionButton(function, "Delete");
            BtnDelete.Click();
        }

        public void EditFunction(Function function)
        {
            _helperClass.ClickMenuItem("Function");
            ClickFunctionButton(function, "Edit");
            ClearFunctionFields();
            SetFunctionData(function);
            ClickSaveButton();
        }

        private void ClearFunctionFields()
        {
            TxtProcessName.Clear();
            DpCategory.Clear();
        }

        private void SetFunctionData(Function function)
        {
            TxtProcessName.SendKeys(function.ProcessName);
            DpCategory.SendKeys(function.Category);
        }

        private Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> GetFunctionTable()
        {
            var table = PropertiesCollection.ChromeDriver.FindElement(By.ClassName("table"));
            var functionDictionary = _helperClass.CreateDictionaryFromTable(table);
            return functionDictionary;
        }

        public int GetFunctionCount()
        {
            return _helperClass.GetTableSize(GetFunctionTable());
        }

        public void CheckFunctionCreated(Function function)
        {
            var functionTable = GetFunctionTable();
            _helperClass.CheckTableItem("Name", function.ProcessName, functionTable);
        }

        private void ClickFunctionButton(Function function, string buttonName)
        {
            var functionTable = GetFunctionTable();
            var functionInstance = _helperClass.GetRowFromTable(functionTable, "Name", function.ProcessName);
           // var dropdownMenu = _helperClass.GetDropdownMenuFromDictionary(functionInstance, "Options");
           // _helperClass.GetButtonInDropdown(dropdownMenu, buttonName);
        }

        public void CheckSectionName(IWebElement sectionHeader, string title)
        {
            var items = sectionHeader.FindElements(By.TagName("td"));
            Assert.AreNotEqual(true, items.Count == 0 || items.Count > 1);
            Assert.AreEqual(true, items[0].Text == title);
        }

        public void CheckSectionData(IWebElement table, List<KeyValuePair<string, string>> sectionData)
        {
            var rows = table.FindElements(By.TagName("tr"));
            Assert.AreEqual(true, rows.Any());

            for (int i = 1; i < rows.Count; i++)
            {
                var data = rows[i].FindElements(By.TagName("td"));
                Assert.AreEqual(true, data.ElementAt(1).Text == sectionData.ElementAt(i - 1).Key);
                Assert.AreEqual(true, data.ElementAt(2).Text == sectionData.ElementAt(i - 1).Value);
            }
        }

    }
}
