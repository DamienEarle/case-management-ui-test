﻿using Cranmore.Briefcase.UI.Test.Models;
using Cranmore.Briefcase.UI.Test.Object_Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Test_Suites
{
    internal class EmployeeTestSuite : EmployeePageObjects
    {
        private readonly HelperClass _helperClass = new HelperClass();

        private void ClickAddEmployeeButton()
        {
            BtnAddEmployee.Click();
        }

        private void ClearEmployeeFields()
        {
            TxtTitle.Clear();
            TxtEmail.Clear();
            TxtFirstName.Clear();
            TxtLastName.Clear();
            TxtJobTitle.Clear();
            TxtUsername.Clear();
            TxtEmail.Clear();
            DpDateStarted.Clear();
            TxtEmployeeNumber.Clear();
        }

        private void ClickAddUserButton()
        {
            BtnAddUser.Click();
        }

        private void ClickSaveUserButton()
        {
            BtnSaveUser.Click();
        }

        private void ClickCancelButton()
        {
            BtnCancel.Click();
        }

        public void ClickDeleteButton()
        {
            BtnDelete.Click();
        }

        public void CreateNewUser(Employee employee)
        {
            ClickAddEmployeeButton();
            SetEmployeeValues(employee);
            _helperClass.SelectFromDropdown("WorkflowRoleId", employee.WorkflowRole);
            _helperClass.SelectFromDropdown("RoleRoleId", employee.UserLevel);
            _helperClass.SelectFromDropdown("FactoryId", employee.Factory);
            _helperClass.SelectFromDropdown("DepartmentId", employee.Department);
            ClickAddUserButton();
        }

        public void CheckEmployeeExists(Employee employee)
        {
            var employeeTable = GetEmployeeTable();

            _helperClass.CheckTableItem("Title", employee.Title, employeeTable);
            _helperClass.CheckTableItem("First Name", employee.FirstName, employeeTable);
            _helperClass.CheckTableItem("Last Name", employee.LastName, employeeTable);
            _helperClass.CheckTableItem("Job Title", employee.JobTitle, employeeTable);
            _helperClass.CheckTableItem("Username", employee.Username, employeeTable);
            _helperClass.CheckTableItem("User Level", employee.UserLevel, employeeTable);
        }

        public void CheckEmployeeDeleted(Employee employee)
        {
            var employeeTable = GetEmployeeTable();
            _helperClass.CheckTableItemNonExistant("Username", employee.Username, employeeTable);
        }
        private Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> GetEmployeeTable()
        {
            var table = PropertiesCollection.ChromeDriver.FindElement(By.ClassName("table"));
            var employeeDictionary = _helperClass.CreateDictionaryFromTable(table);
            return employeeDictionary;
        }

        public int GetEmployeeCount()
        {
            return _helperClass.GetTableSize(GetEmployeeTable());
        }

        public void ClickEmployeeMenuButton(Employee employee, string menuItemName, string menuName = "Menu")
        {
            var tableRow = _helperClass.ClickTableButton(TblEmployees, employee.Username, menuName);
            _helperClass.ClickDropDownMenuItem(tableRow, menuItemName);
        }

        private void ChangeEmployeeDetails(Employee employee)
        {
            employee.Title = "Mrs";
            employee.FirstName = "Test";
            employee.LastName = "User";
            employee.JobTitle = "Testing";
            employee.StaffNumber = "7357";
            employee.WorkflowRole = "Senior Professional Technical Officer";
            employee.DateStarted = DateTime.Today.ToShortDateString();
            SendEmployeeEdit(employee);
        }

        public void SendEmployeeEdit(Employee employee)
        {
            ClearEmployeeFields();
            SetEmployeeValues(employee);
            ClickSaveUserButton();
        }

        private void SetEmployeeValues(Employee employee)
        {
            TxtTitle.SendKeys(employee.Title);
            TxtFirstName.SendKeys(employee.FirstName);
            TxtLastName.SendKeys(employee.LastName);
            TxtJobTitle.SendKeys(employee.JobTitle);
            TxtUsername.SendKeys(employee.Username);
            TxtPassword.SendKeys(employee.Password);
            TxtEmail.SendKeys(employee.EmailAddress);
            DpDateStarted.SendKeys(employee.DateStarted);
            TxtEmployeeNumber.SendKeys(employee.StaffNumber);

            DdlDepartment.SendKeys(employee.Department);
            DdlFactory.SendKeys(employee.Factory);
            DdlUserLevel.SendKeys(employee.UserLevel);
        }


    }
}
