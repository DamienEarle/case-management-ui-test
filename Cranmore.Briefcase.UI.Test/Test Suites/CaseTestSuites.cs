﻿using Cranmore.Briefcase.UI.Test.Object_Models;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace Cranmore.Briefcase.UI.Test.Test_Suites
{
    internal class CaseTestSuites : CasePageObject
    {
        private readonly HelperClass _helperClass = new HelperClass();

        private void ClickCreateButton()
        {
            PropertiesCollection.Wait.Until(p => BtnCreate.Displayed);
            BtnCreate.Click();
        }

        private void ClickCreateOperationButton()
        {
            PropertiesCollection.Wait.Until(p => BtnCreateOperation.Displayed);
            BtnCreateOperation.Click();
        }

        private void ClickCanceButton()
        {
            PropertiesCollection.Wait.Until(p => BtnCancel.Displayed);
            BtnCancel.Click();
        }

        private void ClickReportsButton()
        {
            PropertiesCollection.Wait.Until(p => BtnReports.Displayed);
            BtnReports.Click();
        }

        public void CreateNewCase(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> properties)
        {
            var caseType = _helperClass.GetPropertyValue(properties.Value, "CaseType");
            var caseId = _helperClass.GetPropertyValue(properties.Value, "CaseId");
            if (caseId == null)
                TxtCaseIdentifier.SendKeys(caseId);
            _helperClass.ClickMenuItem("Cases", caseType);
            Thread.Sleep(5000);
            ClickCreateOperationButton();
            Thread.Sleep(5000);
            ClickCreateButton();
            Thread.Sleep(5000);
        }

        public void ClickCaseButton(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> properties)
        {
            Thread.Sleep(7000);
            var buttonName = _helperClass.GetPropertyValue(properties.Value, "ButtonName");
            PropertiesCollection.Wait.Until(p => PropertiesCollection.ChromeDriver.FindElement(By.LinkText(buttonName)).Displayed);
            var updateButton = PropertiesCollection.ChromeDriver.FindElement(By.LinkText(buttonName));
            updateButton.Click();
            Thread.Sleep(7000);
        }

        private void WaitForLock()
        {
            PropertiesCollection.Wait.Until(f => PropertiesCollection.ChromeDriver.FindElement(By.Id("schema_info_lock")).Displayed == false);
        }

        public string CreateCase(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            foreach (var properties in xml)
            {
                switch (properties.Key)
                {
                    case "CreateCase":
                        CreateNewCase(properties);
                        break;
                    case "CaseValue":
                        _helperClass.FillCaseValues(properties, CreateCaseRenderForm);
                        break;
                    case "ClickButton":
                        ClickActionButton(properties.Value.FirstOrDefault());
                        break;
                }
            }
            Thread.Sleep(8000);
            PropertiesCollection.Wait.Until(a => PropertiesCollection.ChromeDriver.FindElement(By.Id("casedata")).Displayed);
            PropertiesCollection.Wait.Until(a => txtCaseTitle.Displayed);
            return txtCaseTitle.Text.Substring(0, 15);
        }

        public void SearchForCase(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml, string caseName)
        {
            XmlElement caseTypeElement = xml.FirstOrDefault(a => a.Key == "CreateCase")
                .Value.FirstOrDefault(b => b.Key == "CaseType")
                .Value;
            string caseType = caseTypeElement.InnerText;

            _helperClass.ClickMenuItem("Cases", caseType);
            PropertiesCollection.Wait.Until(a => txtCaseSearch.Displayed);
            txtCaseSearch.SendKeys(caseName);
            btnCaseSearch.Click();
            Thread.Sleep(10000);
            PropertiesCollection.Wait.Until(a => tblSearchResults.Displayed);
            PropertiesCollection.Wait.Until(a => tblSearchResults.FindElement(By.TagName("tbody")).Displayed);
            var searchBody = tblSearchResults.FindElement(By.TagName("tbody"));
            PropertiesCollection.Wait.Until(a => searchBody.FindElements(By.TagName("tr"))
                .FirstOrDefault().Displayed);
            searchBody.FindElements(By.TagName("tr")).FirstOrDefault().Click();
            PropertiesCollection.Wait.Until(a => tblSearchModal.Displayed);
            tblSearchModal.Click();
        }

        public void UpdateCase(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            foreach (var properties in xml)
            {
                switch (properties.Key)
                {
                    case "UpdateCase":
                        ClickCaseButton(properties);
                        break;
                    case "CaseValue":
                        _helperClass.FillCaseValues(properties, UpdateCaseRenderForm);
                        break;
                    case "ClickButton":
                        ClickActionButton(properties.Value.FirstOrDefault());
                        break;
                }
            }
        }

        public void UpdateOperationBrief(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            foreach (var properties in xml)
            {
                switch (properties.Key)
                {
                    case "UpdateBrief":
                        ClickCaseButton(properties);
                        break;
                    case "BriefDetail":
                        AddResource(properties);
                        break;
                    case "ClickButton":
                        ClickActionButton(properties.Value.FirstOrDefault());
                        break;
                }
            }
        }

        public void AssignResources(IEnumerable<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            foreach(var properties in xml)
            {
                switch (properties.Key)
                {
                    case "AssignResources":
                        ClickCaseButton(properties);
                        break;
                    case "AddResource":
                        AddResource(properties);
                        break;
                    case "ClickButton":
                        ClickActionButton(properties.Value.FirstOrDefault());
                        break;
                }
            }
        }

        public void AddResource(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> xml)
        {
            var assignmentTitle = GetInnerText(xml.Value.FirstOrDefault(a => a.Key == "AssignmentTitle"));
            var resourceValue = GetInnerText(xml.Value.FirstOrDefault(a => a.Key == "ResourceValue"));
            var resourceTitle = GetInnerText(xml.Value.FirstOrDefault(a => a.Key == "ResourceTitle"));
            var actionButtonName = GetInnerText(xml.Value.FirstOrDefault(a => a.Key == "ActionButton"));
            var inputType = GetInnerText(xml.Value.FirstOrDefault(a => a.Key == "InputType"));

            PropertiesCollection.Wait.Until(d => PropertiesCollection.ChromeDriver.FindElement(By.Id("RenderView")).Displayed);
            var renderForm = PropertiesCollection.ChromeDriver.FindElement(By.Id("RenderView"));
            PropertiesCollection.Wait.Until(r => renderForm.FindElements(By.TagName("h2"))
                .FirstOrDefault(x => x.Text.Equals(assignmentTitle)));
            var formHeading = PropertiesCollection.ChromeDriver.FindElements(By.TagName("h2"))
                .FirstOrDefault(x => x.Text.Equals(assignmentTitle));
            var form = formHeading.FindElement(By.XPath("../.."));
            var spanHeading = form.FindElements(By.TagName("span")).FirstOrDefault(p => p.Text == resourceTitle);
            var row = spanHeading.FindElement(By.XPath("../.."));
            IWebElement input;
            switch (inputType)
            {
                case "radio":
                    PropertiesCollection.Wait.Until(
                        d => row.FindElements(By.TagName("span")).FirstOrDefault(x => x.Text.Equals(resourceValue)).Displayed);
                    input = row.FindElements(By.TagName("span")).FirstOrDefault(x => x.Text.Equals(resourceValue));
                    input.Click();
                    break;
                default:
                    var tagType = row.FindElement(By.TagName(inputType));
                    tagType.SendKeys(resourceValue);
                    break;
            }       

            if (actionButtonName != string.Empty)
            {
                var button = form.FindElements(By.TagName("button")).FirstOrDefault(b => b.Text == actionButtonName);
                button.Click();
            }
        }

        private static string GetInnerText(KeyValuePair<string, dynamic> property)
        {
            var xmlElement = property.Value as XmlElement;
            return xmlElement.InnerText;
        }

        public void ClickActionButton(KeyValuePair<string, dynamic> submitPair)
        {
            switch (HelperClass.GetInnerText(submitPair))
            {
                case "Submit":
                    PropertiesCollection.Wait.Until(p => BtnSubmit.Displayed);
                    BtnSubmit.Click();
                    break;
            }
        }
    }
}
