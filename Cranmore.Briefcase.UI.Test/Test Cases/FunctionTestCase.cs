﻿using Cranmore.Briefcase.UI.Test.Models;
using Cranmore.Briefcase.UI.Test.Test_Suites;
using OpenQA.Selenium.Support.UI;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.IE;

namespace Cranmore.Briefcase.UI.Test.Test_Cases
{
    [TestClass]
    public class FunctionTestCase : PropertiesCollection
    {

        private string _url;
        private HelperClass _helperClass;
        private Function _function;
        private SignInTestSuite _signInTestSuite;
        private FunctionTestSuite _functionTestSuite;

        [TestInitialize]
        public void TestSetup()
        {
            ChromeDriver = new InternetExplorerDriver();
            Wait = new WebDriverWait(ChromeDriver, new TimeSpan(0, 0, 30));

            _helperClass = new HelperClass();
            _signInTestSuite = new SignInTestSuite();
            _functionTestSuite = new FunctionTestSuite();

            _function = new Function()
            {
                ProcessName = "Test Process",
                Category = "Enforcement"
            };

            _url = "http://localhost:14792/";
            ChromeDriver.Navigate().GoToUrl(_url);
            ChromeDriver.Manage().Window.Maximize();

            _signInTestSuite.Login();
        }

        [TestMethod]
        public void AddNewFunction()
        {
            _functionTestSuite.AddFunction(_function);
            _functionTestSuite.CheckFunctionCreated(_function);
        }

        [TestMethod]
        public void EditFunction()
        {
            _functionTestSuite.EditFunction(_function);
        }

        [TestMethod]
        public void DeleteFunction()
        {
            _functionTestSuite.DeleteFunction(_function);
        }

        //[TestCleanup]
        //public void Finally()
        //{
        //    ChromeDriver.Close();
        //}
    }
}
