﻿using System;
using Cranmore.Briefcase.UI.Test.Test_Suites;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using XMLEngine;
using System.Configuration;
using System.IO;
using System.Collections;
using NUnit.Framework;
using OpenQA.Selenium.IE;
using System.Reflection;

namespace Cranmore.Briefcase.UI.Test.Test_Cases
{
    [TestClass]
    public class SignInTestCases : PropertiesCollection
    {
        private SignInTestSuite _signInTestSuite;
        private string _url;
        private readonly HelperClass _helperClass = new HelperClass();
        private static XMLParser _xmlParser;
        private static List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> _xml;
        private AppiumTestSuite _appiumTestSuite;

        [TestInitialize]
        public void TestSetup()
        {
           // _url = "http://dev.live-q.com/SignIn"; //Change this
           // ChromeDriver = new InternetExplorerDriver();
           // ChromeDriver.Navigate().GoToUrl(_url);
           // Wait = new WebDriverWait(ChromeDriver, new TimeSpan(0, 0, 30));
            //ChromeDriver.Manage().Window.Maximize();
           // _signInTestSuite = new SignInTestSuite();

            _xml = GetTestCases("SignInAndroid.XML");

            _helperClass.Appium_TestSetup(_xml);
            _appiumTestSuite = new AppiumTestSuite();

        }

        [TestMethod]
        public void Login_Successful()
        {
            _signInTestSuite.Login();
        }

        [TestMethod]
        public void LoginToAppium_Successful()
        {
            //_appiumTestSuite.Signin();
            var tabName = "Operations";
            //_appiumTestSuite.SelectTabItem(tabName);
        }

        //[TestCleanup]
        //public void Finally()
        //{
        //    ChromeDriver.Close();
        //}

        public static List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> //Absolutely mad
            GetTestCases(string xmlName)
        {
            var xmlPath = ConfigurationManager.AppSettings["XMLPath"];

            var signInPath = Assembly.GetExecutingAssembly().Location + "../../../../../" +  xmlPath + "\\" + xmlName; //Ugly line of code. root irectory

            _xmlParser = new XMLParser(signInPath);
            return _xmlParser.ActionValues();
        }

    }
}
