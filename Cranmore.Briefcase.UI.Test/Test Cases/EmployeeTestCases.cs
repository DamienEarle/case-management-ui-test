﻿
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;
using Cranmore.Briefcase.UI.Test.Test_Suites;
using Cranmore.Briefcase.UI.Test.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.IE;

namespace Cranmore.Briefcase.UI.Test.Test_Cases
{
    [TestClass]
    public class EmployeeTestCases : PropertiesCollection
    {

        private string _url;
        private HelperClass _helperClass;

        private SignInTestSuite _signInTestSuite;
        private EmployeeTestSuite _employeeTestSuite;
        private Employee _employee;

        [TestInitialize]
        public void TestSetup()
        {
            ChromeDriver = new InternetExplorerDriver();
            Wait = new WebDriverWait(ChromeDriver, new TimeSpan(0, 0, 30));

            _url = "http://localhost:14792/SignIn";
            _helperClass = new HelperClass();

            _signInTestSuite = new SignInTestSuite();
            _employeeTestSuite = new EmployeeTestSuite();

            _employee = new Employee
            {
                Title = "Mr",
                FirstName = "Test",
                LastName = "User",
                JobTitle = "Test User",
                Username = "TestUser",
                Password = "cranmore@dm1n",
                EmailAddress = "damien@cranmoreconsulting.com",
                DateStarted = DateTime.Today.ToShortDateString(),
                StaffNumber = "0",
                WorkflowRole = "Chief Enforcement Officer",
                UserLevel = "Super User",
                Factory = "DVA",
                Department = "Intelligence"
            };

            ChromeDriver.Navigate().GoToUrl(_url);
            ChromeDriver.Manage().Window.Maximize();

            _signInTestSuite.Login();
            _helperClass.ClickMenuItem("Company", "Users");
        }

        [TestMethod]
        public void CreateNewEmployee()
        {
            var preAddCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.CreateNewUser(_employee);
            _employeeTestSuite.CheckEmployeeExists(_employee);

            var postAddCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, postAddCount - preAddCount == 1);
        }

        private void ChangeEmployeeDetails(Employee employee)
        {
            employee.Title = "Mrs";
            employee.FirstName = "Edit";
            employee.LastName = "Cran";
            employee.JobTitle = "Testing";
            employee.StaffNumber = "7357";
            employee.DateStarted = DateTime.Today.ToShortDateString();
            employee.Username = "EditedName";
            _employeeTestSuite.SendEmployeeEdit(employee);
        }


        [TestMethod]
        public void EditEmployee()
        {
            var preEditCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.ClickEmployeeMenuButton(_employee, "Edit");
            ChangeEmployeeDetails(_employee);
            _employeeTestSuite.CheckEmployeeExists(_employee);

            var postEditCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, postEditCount == preEditCount);
        }

        [TestMethod]
        public void RemoveEmployee()
        {
            var preDeleteCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.ClickEmployeeMenuButton(_employee, "Delete");
            _employeeTestSuite.ClickDeleteButton();
            _employeeTestSuite.CheckEmployeeDeleted(_employee);

            var postDeleteCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, preDeleteCount - postDeleteCount == 1);
        }

        [TestMethod]
        public void FullCoverageOfEmployee()
        {
            var preAddCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.CreateNewUser(_employee);
            _employeeTestSuite.CheckEmployeeExists(_employee);

            var postAddCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, postAddCount - preAddCount == 1);

            var preEditCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.ClickEmployeeMenuButton(_employee, "Edit");
            ChangeEmployeeDetails(_employee);
            _employeeTestSuite.CheckEmployeeExists(_employee);

            var postEditCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, postEditCount == preEditCount);

            var preDeleteCount = _employeeTestSuite.GetEmployeeCount();
            _employeeTestSuite.ClickEmployeeMenuButton(_employee, "Delete");
            _employeeTestSuite.ClickDeleteButton();
            _employeeTestSuite.CheckEmployeeDeleted(_employee);

            var postDeleteCount = _employeeTestSuite.GetEmployeeCount();
            Assert.AreEqual(true, preDeleteCount - postDeleteCount == 1);
        }

        //[TestCleanup]
        //public void Finally()
        //{
        //    ChromeDriver.Close();
        //}
    }
}
