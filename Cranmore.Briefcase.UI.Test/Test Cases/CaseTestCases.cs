﻿using System;
using NUnit.Framework;
using Cranmore.Briefcase.UI.Test.Test_Suites;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using XMLEngine;
using System.Configuration;
using System.IO;
using System.Linq;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;

namespace Cranmore.Briefcase.UI.Test.Test_Cases
{
    [TestFixture]
    public class CaseTestCases : PropertiesCollection
    {
        private string _url;
        private HelperClass _helperClass;

        private SignInTestSuite _signInTestSuite;
        private CaseTestSuites _caseTestSuites;
        private static List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> _xml;
        private static XMLParser _xmlParser;
        private static string _createdCaseName;
        private static string[] _createdCaseNames;

        [SetUp]
        public void TestSetup()
        {
            ChromeDriver = new InternetExplorerDriver();
            Wait = new WebDriverWait(ChromeDriver, new TimeSpan(0, 0, 60));

            _url = "http://dev.live-q.com/Signin";
            _helperClass = new HelperClass();

            _signInTestSuite = new SignInTestSuite();
            _caseTestSuites = new CaseTestSuites();

            ChromeDriver.Navigate().GoToUrl(_url);
            ChromeDriver.Manage().Window.Maximize();
            LoginWithXml("SignIn.XML");
        }

        [Test]
        public void AddOperation()
        {
            _createdCaseName = CreateCaseWithXml("CreateOperation_Standard.XML");
        }

        [Test]
        public void OperationEdit_Ready_Success()
        {
            SearchForCase("CreateOperation_Standard.XML", _createdCaseName);
            UpdateCaseWithXml("UpdateOperation_Ready.XML");
        }
        
        [Test]
        public void OperationEdit_Approve_Success()
        {
            SearchForCase("CreateOperation_Standard.XML", _createdCaseName);
            UpdateCaseWithXml("UpdateOperation_Approve.XML");
        }

        [Test]
        public void OperationEdit_Cancel_Success()
        {
            SearchForCase("CreateOperation_Standard.XML", _createdCaseName);
            UpdateCaseWithXml("UpdateOperation_Cancelled.XML");
        }

        [Test]
        public void OperationBrief_Update_Success()
        {
            SearchForCase("CreateOperation_Standard.XML", _createdCaseName);
            UpdateOperationBrief("OperationBrief.XML");
        }

        [Test]
        public void AssignResource()
        {
            //string url = "http://localhost:14792/Cases/ViewCase/609";
            //ChromeDriver.Navigate().GoToUrl(url);
            SearchForCase("CreateOperation_Standard.XML", _createdCaseName);
            AssignResourcesWithXml("AssignResource.XML");
        }

        [Test]
        public void CreateGoodsOperations_ForTesting()
        {        
            var goodsXml = SignInTestCases.GetTestCases("CreateOperation_Goods.XML");
            int numOfTests = 4;
            CreateCasesForTesting(goodsXml, numOfTests);
        }

        [Test]
        public void CreateTaxiOperations_ForTesting()
        {
            var taxiXml = SignInTestCases.GetTestCases("CreateOperation_Taxi.XML");
            CreateCasesForTesting(taxiXml);
        }

        [Test]
        public void CreateBusOperations_ForTesting()
        {
            var busXml = SignInTestCases.GetTestCases("CreateOperation_Bus.XML");
            CreateCasesForTesting(busXml);
        }

        private void CreateCasesForTesting(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> caseXml, int numOfTests = 4)
        {
            var assignmentXml = SignInTestCases.GetTestCases("AssignResource_TestPrep.XML");
            var updateXml = SignInTestCases.GetTestCases("UpdateOperation_Ready.XML");
            var approveXml = SignInTestCases.GetTestCases("UpdateOperation_Approve.XML");

            for (int i = 0; i < numOfTests; i++)
            {
                var goodsTest = CreateCaseForTesting(caseXml);
                AssignResourcesForTesting(assignmentXml);
                UpdateCaseForTesting(updateXml);
                UpdateCaseForTesting(approveXml);
            }
        }

        private static void GetTestCases()
        {
            var xmlPath = ConfigurationManager.AppSettings["XMLPath"];
            var xmlPaths = Directory.GetFiles(Directory.GetCurrentDirectory() + "../../../.." + "\\" + xmlPath);
            foreach (var xmlValues in xmlPaths)
            {
                _xmlParser = new XMLParser(xmlValues);
                _xml = _xmlParser.ActionValues();
            }
        }

        private void UpdateCaseWithXml(string xmlName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            _caseTestSuites.UpdateCase(_xml);
        }

        private string CreateCaseWithXml(string xmlName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            return _caseTestSuites.CreateCase(_xml);
        }

        private void SearchForCase(string xmlName, string caseName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            _caseTestSuites.SearchForCase(_xml, caseName);
        }

        private void UpdateOperationBrief(string xmlName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            _caseTestSuites.UpdateOperationBrief(_xml);
        }

        private void LoginWithXml(string xmlName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            _signInTestSuite.LoginWithXml(_xml);
        }

        private void AssignResourcesWithXml(string xmlName)
        {
            _xml = SignInTestCases.GetTestCases(xmlName);
            _caseTestSuites.AssignResources(_xml);
        }


        private void AssignResourcesForTesting(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            _caseTestSuites.AssignResources(xml);
        }

        private string CreateCaseForTesting(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            return _caseTestSuites.CreateCase(xml);
        }

        private void SearchForCaseForTesting(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml, string caseName)
        {
            _caseTestSuites.SearchForCase(xml, caseName);
        }

        private void UpdateCaseForTesting(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            _caseTestSuites.UpdateCase(xml);
        }

        //[TearDown]
        //public void Finally()
        //{
        //    ChromeDriver.Close();
        //}
    }
}
