﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cranmore.Briefcase.UI.Test.Models;
using Cranmore.Briefcase.UI.Test.Object_Models;
using Cranmore.Briefcase.UI.Test.Test_Suites;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.UI;
using XMLEngine;

namespace Cranmore.Briefcase.UI.Test.Test_Cases
{
    [TestClass]
    public class EncounterTestCase : PropertiesCollection
    {
        private string _url;
        private HelperClass _helperClass;

        private SignInTestSuite _signInTestSuite;
        private AppiumTestSuite _appiumTestSuite;
        private LiveQSignInTestSuite _liveQSignInTestSuite;
        private OperationTestSuite _operationTestSuite;
        private static XMLParser _xmlParser;
        private static List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> _xml;

        [TestInitialize]
        public void Setup()
        {
            //Wait = new WebDriverWait(ChromeDriver, new TimeSpan(0, 0, 30));
            
            _helperClass = new HelperClass();
            //TODO Remove this static method and maybe put the xml methods into a new class 
            _xml = SignInTestCases.GetTestCases("SignInAndroid.XML");
            _helperClass.Appium_TestSetup(_xml);

            _appiumTestSuite = new AppiumTestSuite();
            _liveQSignInTestSuite = new LiveQSignInTestSuite();
            _operationTestSuite = new OperationTestSuite();
            //_liveQSignInTestSuite.SignInToGood();
        }

        [TestMethod]
        public void CompleteTargetedGoodsEncounter_Success_EncounterComplete()
        {
            _operationTestSuite.OpenOperationsTab();
            //_operationTestSuite.OpenFirstOperationOfType("Targeted Goods Operation");
            _operationTestSuite.SubmitOperation();
            //_operationTestSuite.CreateEncounterFromVRMLookup("123321");
        }

        [TestCleanup]
        public void CleanupTests()
        {
            
        }
    }
}
