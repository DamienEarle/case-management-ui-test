﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace Cranmore.Briefcase.UI.Test
{
    public class LiveQHelperClass
    {
        private void ClickWidgetButton(string name)
        {
            PropertiesCollection.AndroidDriver.FindElement(By.XPath("//android.widget.Button[content-desc='" + name + "']")).Click();
        }

        public void ClickNextButton()
        {
            ClickWidgetButton("Next");
        }

        public void ClickPrevButton()
        {
            ClickWidgetButton("Previous");
        }
        public void ClickSubmitButton()
        {
            ClickWidgetButton("Submit");
        }


        public void ClickCheckboxButton(string fieldName, string value)
        {
            var renderForm = PropertiesCollection.AndroidDriver.FindElementByXPath("id('RenderForm')");
            var views = renderForm.FindElementsByClassName("android.view.View");
            foreach (var view in views)
            {
                if (view.GetAttribute("content-desc") == fieldName)
                {
                    view.Click();
                }
            }


            //PropertiesCollection.Wait.Until(a => PropertiesCollection.AndroidDriver.FindElementByXPath(
            //            "id('RenderForm')/android.view.View[@content-desc='" + fieldName + "']").Displayed);
            //var fields = PropertiesCollection.AndroidDriver.FindElementByXPath("id('RenderForm')/android.view.View[@content-desc='" + fieldName + "']");
        }

        public void CompleteTextField(string fieldName, string value)
        {
            var field = PropertiesCollection.AndroidDriver.FindElementsByClassName("android.view.View").First(a=>a.GetAttribute("content-desc") == fieldName);
        }

        public void ClickTab(string tabName)
        {
            var pnlTab =
                PropertiesCollection.AndroidDriver.FindElement(By.Id("android:id/tabs"));
            foreach (var relativeLayout in pnlTab.FindElements(By.ClassName("android.widget.RelativeLayout")))
            {
                foreach (var textView in relativeLayout.FindElements(By.ClassName("android.widget.TextView")))
                {
                    if (textView.Text == tabName)
                    {
                        textView.Click();
                    }
                }
            }
        }

        public void SubmitOperation()
        {
            ScrollDownBottom();
            ClickCheckboxButton("Briefing Delivered*", "Yes");
            //ClickSubmitButton();
        }

        private void ScrollDownBottom()
        {
            //TODO Put a check to make sure its at bottom
            var size = PropertiesCollection.AndroidDriver.Manage().Window.Size;
            int startX = (int) (size.Width * 0.80);
            int startY = (int) (size.Height * 0.80);
            int endX = startX;
            int endY = (int)(size.Width * 0.20);
            int durationMs = 1000;

            PropertiesCollection.AndroidDriver.Swipe(startX, startY, endX, endY, durationMs);
        }
    }
}
