﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace Cranmore.Briefcase.UI.Test
{
    public class PropertiesCollection
    {
        public static IWebDriver ChromeDriver { get; set; }
        public static AndroidDriver<AppiumWebElement> AndroidDriver { get; set; }
        public static WebDriverWait Wait { get; set; }
        public static DesiredCapabilities DesiredCapabilities { get; set; }
    }
}
