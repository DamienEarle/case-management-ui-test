﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Models
{
    public class Employee
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string JobTitle { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string EmailAddress { get; set; }

        public string DateStarted { get; set; }

        public string StaffNumber { get; set; }

        public string WorkflowRole { get; set; }

        public string UserLevel { get; set; }

        public string Factory { get; set; }

        public string Department { get; set; }

    }
}
