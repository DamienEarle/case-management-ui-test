﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Models
{
    public class Function
    {
        public string ProcessName { get; set; }

        public string Category { get; set; }
    }
}
