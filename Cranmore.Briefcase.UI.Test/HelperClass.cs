﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Xml;
using System.Threading;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System.Configuration;

namespace Cranmore.Briefcase.UI.Test
{
    public class HelperClass
    {
        public void Appium_TestSetup(List<KeyValuePair<string, List<KeyValuePair<string, dynamic>>>> xml)
        {
            PropertiesCollection.DesiredCapabilities = new DesiredCapabilities();
            foreach (var properties in xml.FirstOrDefault(x => x.Key == "devicesetup").Value)
            {
                var value = properties.Value as XmlElement;
                PropertiesCollection.DesiredCapabilities.SetCapability(properties.Key, value.InnerText);
            }
            PropertiesCollection.AndroidDriver = new AndroidDriver<AppiumWebElement>(new Uri(ConfigurationManager.AppSettings["AppiumURL"]), PropertiesCollection.DesiredCapabilities);
        }

        public void ClickMenuItem(string mainMenuItemName, string menuItemName = null)
        {
            Thread.Sleep(3000);
            PropertiesCollection.Wait.Until(d => PropertiesCollection.ChromeDriver.FindElement(By.Id("main-menu")).Displayed);
            var mainMenu = PropertiesCollection.ChromeDriver.FindElement(By.Id("main-menu"));
            foreach (var mainMenuListItem in mainMenu.FindElements(By.TagName("li")))
            {
                if (mainMenuListItem.Text.Contains(mainMenuItemName))
                {
                    Thread.Sleep(1000);
                    if (PerformMenuItemClick(menuItemName, mainMenuListItem)) return;
                    else
                    {
                        Console.WriteLine("Returned false looking for: " + mainMenuItemName + " " + menuItemName);
                    };
                }
            }
        }
        private static bool PerformMenuItemClick(string menuItemName, IWebElement mainMenuListItem)
        {
            var builder = new Actions(PropertiesCollection.ChromeDriver);
            var moveTo = builder.MoveToElement(mainMenuListItem).Click().Build();
            moveTo.Perform();

            if (menuItemName == null)
            {
                mainMenuListItem.Click();
                return true;
            }

            PropertiesCollection.Wait.Until(
                d => PropertiesCollection.ChromeDriver.FindElement(By.TagName("li")).Displayed);

            foreach (var menuItem in mainMenuListItem.FindElements(By.TagName("li")))
            {
                if (menuItem.Text == menuItemName)
                {
                    menuItem.Click();
                    return true;
                }
            }
            return false;
        }

        public void CheckTableItem(string columnTitle, string cellData, Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> table)
        {
            var rows = table.FirstOrDefault(a => a.Key.Text == columnTitle).Value;
            Assert.AreNotEqual(null, rows);
            Assert.AreEqual(true, rows.Select(a => a.Text).Contains(cellData));
        }

        public static string GetInnerText(KeyValuePair<string, dynamic> property)
        {
            var xmlElement = property.Value as XmlElement;
            return xmlElement.InnerText;
        }

        public void CheckTableItemNonExistant(string columnTitle, string cellData, Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> table)
        {
            var rows = table.FirstOrDefault(a => a.Key.Text == columnTitle).Value;
            Assert.AreNotEqual(null, rows);
            Assert.AreEqual(false, rows.Select(a => a.Text).Contains(cellData));
        }

        public Dictionary<IWebElement, IWebElement> GetRowFromTable(IDictionary<IWebElement, IReadOnlyCollection<IWebElement>> table, string field, string searchString)
        {
           var employee = new Dictionary<IWebElement, IWebElement>();
            var valueIndex = table.First(t => t.Key.Text == field).Value
                .ToList()
                .FindIndex((a => a.Text == searchString));

            foreach (var pair in table)
            {
                employee.Add(pair.Key, pair.Value.ElementAt(valueIndex));
            }
            return employee;
        }

        public void GetButtonInDropdown(IWebElement dropdownMenu, string buttonText)
        {
            var dropButton = dropdownMenu.FindElement(By.ClassName("btn-group"));
            dropButton.Click();
            var menuItems = dropdownMenu.FindElements(By.TagName("ul"));
            Assert.AreNotEqual(null, menuItems);
            foreach (var item in menuItems)
            {
                var links = item.FindElements(By.TagName("a"));
                Assert.AreNotEqual(null, links);
                links.Where(l => l.Text == buttonText).ToList()
                    .ForEach(i => i.Click());
            }
        }

        public static void HighlightElement(IWebElement element, IWebDriver driver)
        {
            var javaScriptDriver = (IJavaScriptExecutor)driver;
            const string highlightJavascript = @"arguments[0].style.cssText = ""border-width: 3px; border-style: solid; border-color: green"";";
            javaScriptDriver.ExecuteScript(highlightJavascript, new object[] { element });
        }


        public IWebElement ClickTableButton(IWebElement table, string username, string buttonName)
        {
            foreach (var row in table.FindElements(By.TagName("tr")))
            {
                foreach (var cell in row.FindElements(By.TagName("td")))
                {
                    if (cell.Text != username) continue;
                    foreach (var button in row.FindElements(By.TagName("button")))
                    {
                        if (button.Text != buttonName) continue;
                        button.Click();
                        return row;
                    }
                }
            }
            return null;
        }

        public void ClickDropDownMenuItem(IWebElement row, string listItemName)
        {
            var unorderList = row.FindElement(By.TagName("ul"));
            foreach (var listItem in unorderList.FindElements(By.TagName("li")))
            {
                if (listItem.Text == listItemName)
                {
                    listItem.Click();
                    return;
                }
            }
        }


        //public IWebElement GetDropdownMenuFromDictionary(Dictionary<IWebElement, IWebElement> table, string menuHeading)
        //{
        //    var items = table.Values.Where(a => a.FindElements(By.TagName("td")) != null);
        //    var menu = items.FirstOrDefault();

        //    var menu = menu.FindElement(By.ClassName("dropdown-menu"));
        //    GetButtonInDropdown(menu, menuHeading);
        //    return null;
        //}

        public int GetTableSize(Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> table)
        {
            return table.Select(item => item.Value.Count).Concat(new[] { 0 }).Max();
        }

        public Dictionary<IWebElement, IReadOnlyCollection<IWebElement>> CreateDictionaryFromTable(IWebElement baseTable)
        {
            var associatedCells = new Dictionary<IWebElement, IReadOnlyCollection<IWebElement>>();

            var tableBody = baseTable.FindElement(By.TagName("tbody"));
            var headRow = tableBody.FindElements(By.TagName("tr")).ElementAt(0).
                FindElements(By.TagName("th"));
            var bodyRows = tableBody.FindElements(By.TagName("tr")).Skip(1);

            for (var headIndex = 0; headIndex < headRow.Count; headIndex++)
            {
                var cells = new Collection<IWebElement>();
                for (var bodyIndex = 0; bodyIndex < bodyRows.Count(); bodyIndex++)
                {
                    var row = bodyRows.ElementAt(bodyIndex).FindElements(By.TagName("td"));
                    cells.Add(row.ElementAt(headIndex));
                }
                associatedCells.Add(headRow.ElementAt(headIndex), cells);
            }
            return associatedCells;
        }

        public void SelectFromDropdown(string id, string selection = "")
        {
            var dropdown = PropertiesCollection.ChromeDriver.FindElement(By.Id(id));
            var selectElement = new SelectElement(dropdown);
            selectElement.SelectByText(selection);
        }

        public string GetPropertyValue(IEnumerable<KeyValuePair<string, dynamic>> properties, string item)
        {
            XmlElement xmlElement = properties.Where(x => x.Key == item).Select(x => x.Value).FirstOrDefault();
            if (xmlElement != null)
                return xmlElement.InnerText;
            return "";
        }

        //TODO : Clean code this badboy
        public void FillCaseValues(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> properties, IWebElement renderForm)
        {
            var controlTitlePair = GetValuePairByKey(properties, "Title");
            PropertiesCollection.Wait.Until(
                d => renderForm.FindElement(By.XPath(".//span[contains(text(), '" + HelperClass.GetInnerText(controlTitlePair) + "')]")).Displayed);
            var rowTitleElement = renderForm.FindElement(By.XPath(".//span[contains(text(), '" + HelperClass.GetInnerText(controlTitlePair) + "')]"));
            PropertiesCollection.Wait.Until(
                d => rowTitleElement.FindElement(By.XPath("../..")).Displayed);
            var parentElement = rowTitleElement.FindElement(By.XPath("../.."));
            var controlTypePair = GetValuePairByKey(properties, "ControlType");

            var valuePair = GetValuePairByKey(properties, "Value");
            string inputText = GetInnerText(valuePair);
            string targetTagType = GetInnerText(controlTypePair);
            InsertValueIntoTag(inputText, targetTagType, parentElement);
        }

        public void InsertValueIntoTag(string inputText, string targetTagType, IWebElement parentElement)
        {
            IWebElement input;          
            switch (targetTagType)
            {
                case "radio":
                    PropertiesCollection.Wait.Until(
                        d => parentElement.FindElements(By.TagName("span")).FirstOrDefault(x => x.Text.Equals(inputText)).Displayed);
                    input = parentElement.FindElements(By.TagName("span")).FirstOrDefault(x => x.Text.Equals(inputText));
                    input.Click();
                    break;
                case "select":
                    input = parentElement.FindElement(By.TagName(targetTagType));
                    var options = input.FindElements(By.TagName("option"));
                    foreach (var option in options)
                    {
                        if (option.Text.Equals(inputText))
                        {
                            option.Click();
                            break;
                        }
                    }
                    break;
                case "date":
                    input = parentElement.FindElement(By.TagName("input"));
                    if (inputText == string.Empty)
                    {
                        var todaysDate = DateTime.Today.ToShortDateString();
                        inputText = todaysDate;
                    }
                    input.SendKeys(inputText);
                    break;
                default:
                    PropertiesCollection.Wait.Until(p => parentElement.FindElement(By.TagName(targetTagType)).Displayed);
                    input = parentElement.FindElement(By.TagName(targetTagType));
                    input.SendKeys(inputText);
                    break;
            }
        }

        public KeyValuePair<string, dynamic> GetValuePairByKey(KeyValuePair<string, List<KeyValuePair<string, dynamic>>> properties, string key)
        {
            return properties.Value.FirstOrDefault(p => p.Key == key);
        }


    }
}
