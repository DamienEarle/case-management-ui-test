﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class EmployeePageObjects
    {
        public EmployeePageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.ChromeDriver, this);
        }

        [FindsBy(How = How.XPath, Using = "//*[@id='body']/section/h2/div/a")]
        protected IWebElement BtnAddEmployee { get; set; }

        [FindsBy(How = How.Id, Using = "Title")]
        protected IWebElement TxtTitle { get; set; }

        [FindsBy(How = How.Id, Using = "FirstName")]
        protected IWebElement TxtFirstName { get; set; }

        [FindsBy(How = How.Id, Using = "LastName")]
        protected IWebElement TxtLastName { get; set; }

        [FindsBy(How = How.Id, Using = "JobTitle")]
        protected IWebElement TxtJobTitle { get; set; }

        [FindsBy(How = How.Id, Using = "Username")]
        protected IWebElement TxtUsername { get; set; }

        [FindsBy(How = How.Id, Using = "Password")]
        protected IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "UserEmail")]
        protected IWebElement TxtEmail { get; set; }

        [FindsBy(How = How.Id, Using = "DateStarted")]
        protected IWebElement DpDateStarted { get; set; }

        [FindsBy(How = How.Id, Using = "EmployeeNumber")]
        protected IWebElement TxtEmployeeNumber { get; set; }

        [FindsBy(How = How.Id, Using = "RoleRoleId")]
        protected IWebElement DdlUserLevel { get; set; }

        [FindsBy(How = How.Id, Using = "WorkflowRoleId")]
        protected IWebElement DdlWorkflowRole { get; set; }

        [FindsBy(How = How.Id, Using = "FactoryId")]
        protected IWebElement DdlFactory { get; set; }

        [FindsBy(How = How.Id, Using = "DepartmentId")]
        protected IWebElement DdlDepartment { get; set; }

        [FindsBy(How = How.Id, Using = "Create")]
        protected IWebElement BtnAddUser { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='body']/section/form/p/a")]
        protected IWebElement BtnCancel { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\'body']/section/div/div[2]/form/fieldset/p/input")]
        protected IWebElement BtnSaveUser { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='body']/section/table")]
        protected IWebElement TblEmployees { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"body\"]/section/div/div[2]/form/p/input")]
        protected IWebElement BtnDelete { get; set; }
    }
}