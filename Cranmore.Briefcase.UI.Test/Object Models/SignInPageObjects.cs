﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class SignInPageObjects
    {

        public SignInPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.ChromeDriver, this);
        }

        [FindsBy(How = How.Id, Using = "Username")]
        protected IWebElement TxtUsername { get; set; }

        [FindsBy(How = How.Id, Using = "Password")]
        protected IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='body']/section/form/fieldset/p/input")]
        protected IWebElement BtnLogIn { get; set; }


    }
}
