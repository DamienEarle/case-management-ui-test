﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class AppiumPageObjects
    {
        //These are likely wrong, but see what they use when it breaks
        public AppiumPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.AndroidDriver, this);
        }

        [FindsBy(How = How.Id, Using = "textView2")]
        protected IWebElement LabelSignInMessage { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/editText2")]
        protected IWebElement TxtUsername { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/editText1")]
        protected IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/btnConnect")]
        protected IWebElement BtnSubmit { get; set; }

        [FindsBy(How = How.Id, Using = "android:id/tabs")]
        protected IWebElement BtnTabWidget { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='FormListView']/div/div/table")]
        protected IWebElement Table { get; set; }

        [FindsBy(How = How.ClassName, Using = "android.widget.GridView")]
        protected IWebElement GridView { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']")]
        protected IWebElement ObjRenderForm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']/fieldset/div[2]/input")]
        protected IWebElement BtnNext { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']/fieldset/div[2]/input[2]")]
        protected IWebElement BtnSubmitForm { get; set; }
    }
}
