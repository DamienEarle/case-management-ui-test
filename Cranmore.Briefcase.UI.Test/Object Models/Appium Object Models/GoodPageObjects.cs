﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class GoodPageObjects
    {
        public GoodPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.AndroidDriver, this);     
        }

        [FindsBy(How = How.Id, Using = "COM_GOOD_GD_EPROV_EMAIL_FIELD")]
        protected IWebElement TxtGoodEmailAddress { get; set; }

        [FindsBy(How = How.Id, Using = "editTextForPin1")]
        protected IWebElement TxtGoodAccessKey { get; set; }

        [FindsBy(How = How.Id, Using = "COM_GOOD_GD_EPROV_ACCESS_BUTTON")]
        protected IWebElement BtnConfirmGoodAccess { get; set; }

        [FindsBy(How = How.Id, Using = "COM_GOOD_GD_LOGIN_VIEW_PASSWORD_FIELD")]
        protected IWebElement TxtGoodPassword { get; set; }

        //[FindsBy(How = How.Id, Using = "")]
        //protected IWebElement BtnConfirmGoodPassword { get; set; }


    }
}
