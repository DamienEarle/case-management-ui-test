﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class OperationPageObjects
    {

        public OperationPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.AndroidDriver, this);
        }

        [FindsBy(How=How.Id, Using = "android:id/tabs")]
        protected IWebElement CaseMenuTab { get; set; }

        [FindsBy(How = How.XPath, Using = "//android.view.View[@resource-id='RenderForm']")]
        protected IWebElement OperationRenderForm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']/View/GridView)]")]
        protected IWebElement MyOperationList { get; set; }

        [FindsBy(How = How.Id, Using = "COM_GOOD_GD_LOGIN_VIEW_PASSWORD_FIELD")]
        protected IWebElement TxtBBDPassword { get; set; }

        [FindsBy(How = How.Id, Using = "COM_GOOD_GD_EPROV_ACCESS_BUTTON")]
        protected IWebElement BtnBBDGo { get; set; }

        [FindsBy(How = How.Id, Using = "textView2")]
        protected IWebElement LabelSignInMessage { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/editText2")]
        protected IWebElement TxtUsername { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/editText1")]
        protected IWebElement TxtPassword { get; set; }

        [FindsBy(How = How.Id, Using = "com.phonegap.liveq3:id/btnConnect")]
        protected IWebElement BtnSubmit { get; set; }

        [FindsBy(How = How.Id, Using = "android:id/tabs")]
        protected IWebElement BtnTabWidget { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='FormListView']/div/div/table")]
        protected IWebElement Table { get; set; }

        [FindsBy(How = How.ClassName, Using = "android.widget.GridView")]
        protected IWebElement GridView { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']")]
        protected IWebElement ObjRenderForm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']/fieldset/div[2]/input")]
        protected IWebElement BtnNext { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='RenderForm']/fieldset/div[2]/input[2]")]
        protected IWebElement BtnSubmitForm { get; set; }

    }
}
