﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    class CasePageObject : SignInPageObjects
    {
        public CasePageObject()
        {
            PageFactory.InitElements(PropertiesCollection.ChromeDriver, this);
        }

        [FindsBy(How = How.Id, Using = "CaseName")]
        protected IWebElement TxtCaseIdentifier { get; set; }

        //[FindsBy(How = How.Id, Using = "Reports")]
        //protected IWebElement TxtDate { get; set; }

        //[FindsBy(How = How.CssSelector, Using = "//div[@role='tablist']//a")]
        //protected IWebElement TxtCaseOriginator { get; set; }

        [FindsBy(How = How.Name, Using = "CreateNew")]
        protected IWebElement BtnCreateOperation { get; set; }

        [FindsBy(How = How.Name, Using = "Create")]
        protected IWebElement BtnCreate { get; set; }

        [FindsBy(How = How.Name, Using = "Cancel")]
        protected IWebElement BtnCancel { get; set; }

        [FindsBy(How = How.Name, Using = "Reports")]
        protected IWebElement BtnReports { get; set; }

        [FindsBy(How = How.CssSelector, Using = "//div[@role='tablist']//a")]
        protected IWebElement BtnTabs { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"RenderForm\"]/fieldset/div[2]/input")]
        protected IWebElement BtnSubmit { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"wrapper_1\"]/div[2]")]
        protected IWebElement CreateCaseRenderForm { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"wrapper_0\"]/div[2]")]
        protected IWebElement UpdateCaseRenderForm { get; set; }

        [FindsBy(How = How.Id, Using = "searchBox")]
        protected IWebElement txtCaseSearch { get; set; }

        [FindsBy(How = How.Id, Using = "searchBtn")]
        protected IWebElement btnCaseSearch { get; set; }

        [FindsBy(How = How.Id, Using = "docs")]
        protected IWebElement tblSearchResults { get; set; }

        [FindsBy(How = How.Id, Using = "casedataresult")]
        protected IWebElement tblSearchModal { get; set; }
        
        [FindsBy(How = How.Id, Using = "caseNameTitle")]
        protected IWebElement txtCaseTitle { get; set; }

    }
}
