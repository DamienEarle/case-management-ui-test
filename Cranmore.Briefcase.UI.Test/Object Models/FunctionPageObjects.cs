﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cranmore.Briefcase.UI.Test.Object_Models
{
    public class FunctionPageObjects
    {
        public FunctionPageObjects()
        {
            PageFactory.InitElements(PropertiesCollection.ChromeDriver, this);
        }

        [FindsBy(How = How.Id, Using = "FunctionName")]
        protected IWebElement TxtProcessName { get; set; }

        [FindsBy(How = How.Id, Using = "FunctionCategoryId")]
        protected IWebElement DpCategory { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"body\"]/section/form/div/div[2]/fieldset/div[5]/input")]
        protected IWebElement BtnAddFunction { get; set; }
        
        [FindsBy(How = How.LinkText, Using = "Cancel")]
        protected IWebElement BtnCancel { get; set; }

        [FindsBy(How = How.CssSelector, Using = "@value='Search'")]
        protected IWebElement BtnDelete { get; set; }

        [FindsBy(How = How.CssSelector, Using = "@value='Save Function'")]
        protected IWebElement BtnSave { get; set; }
    }
}
